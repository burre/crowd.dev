<?php

class ReportController extends Controller
{
	public function actionIndex()
	{
        $this->customPageTitle = 'Звіти';

        $criteria = new CDbCriteria();
        $criteria->addInCondition('status', ['completed', 'early']);
        $criteria->order = 'end_date DESC';
        $criteria->limit = 2;

        if (!$projects = Yii::app()->cache->get('projects_completed')) {
            if ($projects = Project::model()->findAll($criteria)) {
                Yii::app()->cache->set('projects_completed', $projects);
            }
        }

        $this->render(
            'index',
            [
                'projects' => $projects,
            ]
        );
	}

	public function actionView($id=0)
	{
        $this->customPageTitle = 'Звіт';

        /** @var $project Project */
        $project = Yii::app()->cache->get('project'.$id);
        if (!$project) {
            if ($project = Project::model()->findByPk((int)$id)) {
                Yii::app()->cache->set('project'.$id, $project);
            }
        }

        if (!$id || !$project) {
            throw new CHttpException(404, 'Данная страница не найдена.');
        }

        $this->render(
            'view',
            [
                'project' => $project,
            ]
        );
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}