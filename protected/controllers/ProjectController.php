<?php

class ProjectController extends Controller
{
    public function actionIndex($status = 'inprogress')
    {
        $this->customPageTitle = 'Проекти';

        $enabledStatuses = Project::getEnabledStatuses();
        if (!in_array($status, $enabledStatuses)) {
            throw new CHttpException(404, 'Данная страница не найдена.');
        }

        $limit = Yii::app()->params['projectsOnHomePage'];
        $criteria = new CDbCriteria();
        $criteria->addInCondition('status', ['inprogress']);
        $criteria->order = 'start_date ASC';
        $criteria->limit = $limit;

        $count=Project::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = $limit;
        $pages->applyLimit($criteria);

        if (!$projects = Yii::app()->cache->get('projects_'.$status)) {
            if ($projects = Project::model()->findAll($criteria)) {
                Yii::app()->cache->set('projects_'.$status, $projects);
            }
        }

        $this->render(
            'index',
            [
                'projects' => $projects,
                'pages' => $pages,
            ]
        );
    }

    public function actionView($id = 0)
    {
        $this->customPageTitle = 'Проект';

        /** @var $project Project */
        $project = Yii::app()->cache->get('project'.$id);
        if (!$project) {
            if ($project = Project::model()->findByPk((int)$id)) {
                Yii::app()->cache->set('project'.$id, $project);
            }
        }

        if (!$id || !$project) {
            throw new CHttpException(404, 'Данная страница не найдена.');
        }

        $donate = new DonateForm();
        $donate->project_id = $project->id;

        $this->render(
            'view',
            [
                'project' => $project,
                'donate'  => $donate,
            ]
        );
    }

    public function actionDonate()
    {
        if (isset($_POST['DonateForm'])) {
            $donate = new DonateForm();
            $donate->setAttributes($_POST['DonateForm']);
            $donate->comments = $_POST['DonateForm']['comments'];

            if ($donate->paymethod == 'portmone' || $donate->paymethod == 'on_delivery') {
                $retVal = array(
                    'sum' => $donate->sum,
                    'result' => $donate->saveAsPayment() ? 'success' : 'error',
                    'redirect' => $donate->paymethod,
                );
            } elseif ($donate->paymethod == 'cash') {
                $retVal = array(
                    'sum' => $donate->sum,
                    'result' => 'success',
                    'redirect' => $donate->paymethod,
                );
            } elseif (!$donate->paymethod || !$donate->shipmethod) {
                $retVal = array(
                    'sum' => $donate->sum,
                    'result' => 'error',
                    'redirect' => '',
                );
            }

            echo CJSON::encode(CHtml::encodeArray($retVal));
            Yii::app()->end();
        }
    }

    public function filters() {
        return array(
            'ajaxOnly + donate',
        );
    }

    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}