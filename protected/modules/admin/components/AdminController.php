<?php
 /**
 * Project:   crowd.dev
 * File:      AdminController.php
 * Date:      29.09.14
 *
 * @package   package
 * @version   1.0
 * @copyright Copyright (c) 2013 Victor Burre
 * @link      http://
 */


class AdminController extends CController {
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu=array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs=array();

    /**
     * Here we include client JS and CSS files.
     *
     * @param CAction $action
     *
     * @return bool
     */
    public function beforeAction($action)
    {
        if( parent::beforeAction($action) ) {
            /* @var $cs CClientScript */
            $cs = Yii::app()->clientScript;
            $base = Yii::app()->request->baseUrl;

            //$cs->registerPackage('jquery-ui');
            $cs->registerCssFile($base . '/css/screen.css', "screen, projection");
            $cs->registerCssFile($base . '/css/print.css', "print");
            $cs->registerCssFile($base . '/css/main.css');
            $cs->registerCssFile($base . '/css/form.css');
            return true;
        }
        return false;
    }
}