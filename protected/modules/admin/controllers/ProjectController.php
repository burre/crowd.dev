<?php

class ProjectController extends AdminController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$project=new Project;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

        // write down project
		if(isset($_POST['Project']))
		{
            if(isset($_FILES['files']) && !empty($_FILES['files']))
            {
                //upload new files
                $images = [];
                $path = realpath(Yii::app()->getBasePath().'/../'.Yii::app()->params['uploadDirProjects']).'/';
                foreach($_FILES['files']['name'] as $key=>$filename) {
                    if (!$filename) break; // if files was not uploaded
                    move_uploaded_file($_FILES['files']['tmp_name'][$key], $path . $filename);
                    $images[] = Yii::app()->params['uploadDirProjects'].'/'.$filename;
                }

                $project->images = json_encode($images);
            }

            // prepare video list
            if ($_POST['Project']['videos']) {
                $project->videos = json_encode(explode("\n", $_POST['Project']['videos']));
            }

            $project->attributes=$_POST['Project'];
            $created = $project->save();
		}

        // write down rewards list
        if(isset($_POST['ProjectRewards']['reward_id']))
        {
            foreach ($_POST['ProjectRewards']['reward_id'] as $val) {
                if ($val) {
                    $modelReward = new ProjectRewards();
                    $modelReward->project_id = $project->id;
                    $modelReward->reward_id = $val;
                    $modelReward->save();
                }
            }
        }

        // write down partners list
        if(isset($_POST['ProjectPartners']['partner_id']))
        {
            foreach ($_POST['ProjectPartners']['partner_id'] as $val) {
                if ($val) {
                    $modelPartner = new ProjectPartners();
                    $modelPartner->project_id = $project->id;
                    $modelPartner->partner_id = $val;
                    $modelPartner->save();
                }
            }
        }

        if(isset($created) && $created)
            $this->redirect(array('view','id'=>$project->id));

		$this->render('create',array(
			'model'=>$project,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$project=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($project);

        // update project
		if(isset($_POST['Project']))
		{
            // prepare video list
            if ($_POST['Project']['videos']) {
                $project->videos = json_encode(explode("\n", $_POST['Project']['videos']));
            }

			$project->attributes=$_POST['Project'];
            $created = $project->save();
		} else {
            if ($project->videos) {
                $project->videos = implode("\n", $project->getVideos());
            }
        }

        // update rewards list
        if(isset($_POST['ProjectRewards']['reward_id']))
        {
            foreach ($_POST['ProjectRewards']['reward_id'] as $val) {
                if ($val) {
                    $modelReward = new ProjectRewards();
                    $modelReward->project_id = $project->id;
                    $modelReward->reward_id = $val;
                    $modelReward->save();
                }
            }
        }

        if(isset($_POST['ProjectRewards']['reward_id']))
        {
            $rewards = ProjectRewards::model()->findAll('project_id=:project_id', array('project_id'=>$project->id));

            if (!empty($rewards)) {
                foreach ($rewards as $reward) {
                    if (in_array($reward->reward_id, $_POST['ProjectRewards']['reward_id'])) {
                        // delete from array
                        $k = array_search($reward->reward_id, $_POST['ProjectRewards']['reward_id']);
                        if (isset($_POST['ProjectRewards']['reward_id'][$k]))
                            unset($_POST['ProjectRewards']['reward_id'][$k]);
                    } else {
                        // delete from DB
                        $reward->delete();
                    }
                }
            }

            // all of the rest is new partners
            if (!empty($_POST['ProjectRewards']['reward_id'])) {
                foreach ($_POST['ProjectRewards']['reward_id'] as $val) {
                    if ($val) {
                        $modelReward = new ProjectRewards();
                        $modelReward->project_id = $project->id;
                        $modelReward->reward_id = $val;
                        $modelReward->save();
                    }
                }
            }
        }

        // update partners list
        if(isset($_POST['ProjectPartners']['partner_id']))
        {
            $partners = ProjectPartners::model()->findAll('project_id=:project_id', array('project_id'=>$project->id));

            if (!empty($partners)) {
                foreach ($partners as $partner) {
                    if (in_array($partner->partner_id, $_POST['ProjectPartners']['partner_id'])) {
                        // delete from array
                        $k = array_search($partner->partner_id, $_POST['ProjectPartners']['partner_id']);
                        if (isset($_POST['ProjectPartners']['partner_id'][$k]))
                            unset($_POST['ProjectPartners']['partner_id'][$k]);
                    } else {
                        // delete from DB
                        $partner->delete();
                    }
                }
            }

            // all of the rest is new partners
            if (!empty($_POST['ProjectPartners']['partner_id'])) {
                foreach ($_POST['ProjectPartners']['partner_id'] as $val) {
                    if ($val) {
                        $modelPartner = new ProjectPartners();
                        $modelPartner->project_id = $project->id;
                        $modelPartner->partner_id = $val;
                        $modelPartner->save();
                    }
                }
            }
        }

        if(isset($created) && $created)
            $this->redirect(array('view','id'=>$project->id));

		$this->render('update',array(
			'model'=>$project,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Project');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Project('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Project']))
			$model->attributes=$_GET['Project'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Project the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Project::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Project $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='project-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
