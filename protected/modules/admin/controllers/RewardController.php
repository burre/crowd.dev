<?php

class RewardController extends AdminController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Reward;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Reward']))
		{
            $model->attributes=$_POST['Reward'];

            if(isset($_FILES['files']))
            {
                //upload new files
                $path = realpath(Yii::app()->getBasePath().'/../'.Yii::app()->params['uploadDirRewards']).'/';
                foreach($_FILES['files']['name'] as $key=>$filename) {
                    move_uploaded_file($_FILES['files']['tmp_name'][$key], $path . $filename);
                    $model->images = Yii::app()->params['uploadDirRewards'].'/'.$filename;
                }
            }


            if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Reward']))
		{
            $model->attributes=$_POST['Reward'];

            if(isset($_FILES['files']))
            {
                // delete old files
                $oldfFile = realpath(Yii::app()->getBasePath()).'/../'.$model->images;
                if (file($oldfFile)) unlink($oldfFile);

                //upload new files
                $path = realpath(Yii::app()->getBasePath().'/../'.Yii::app()->params['uploadDirRewards']).'/';
                foreach($_FILES['files']['name'] as $key=>$filename) {
                    move_uploaded_file($_FILES['files']['tmp_name'][$key],$path.$filename);
                    $model->images = Yii::app()->params['uploadDirRewards'].'/'.$filename;
                }
            }

            if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Reward');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Reward('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Reward']))
			$model->attributes=$_GET['Reward'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Reward the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Reward::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Reward $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='reward-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    /**
     * @return array filename
     */
    public function findFiles()
    {
        return array_diff(scandir(Yii::app()->params['uploadDirRewards']), array('.', '..'));
    }
}
