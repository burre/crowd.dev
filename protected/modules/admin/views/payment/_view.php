<?php
/* @var $this PaymentController */
/* @var $data Payment */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sum')); ?>:</b>
	<?php echo CHtml::encode($data->sum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('card4digits')); ?>:</b>
    <?php echo CHtml::encode($data->card4digits); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('project_id')); ?>:</b>
	<?php echo CHtml::encode($data->project_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reward_id')); ?>:</b>
	<?php echo CHtml::encode($data->reward_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pay_method_id')); ?>:</b>
	<?php echo CHtml::encode($data->pay_method_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('delivery_method_id')); ?>:</b>
	<?php echo CHtml::encode($data->delivery_method_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('reward_status')); ?>:</b>
	<?php echo CHtml::encode($data->reward_status); ?>
	<br />

	*/ ?>

</div>