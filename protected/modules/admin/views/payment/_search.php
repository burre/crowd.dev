<?php
/* @var $this PaymentController */
/* @var $model Payment */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sum'); ?>
		<?php echo $form->textField($model,'sum',array('size'=>10,'maxlength'=>10)); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model, 'status', array(
                0=>'Оплачен',
                1=>'Не оплачен',
            )); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'card4digits'); ?>
        <?php echo $form->textField($model,'card4digits',array('size'=>4,'maxlength'=>4)); ?>
    </div>

    <div class="row">
		<?php echo $form->label($model,'project_id'); ?>
		<?php echo $form->textField($model,'project_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reward_id'); ?>
		<?php echo $form->textField($model,'reward_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pay_method_id'); ?>
		<?php echo $form->textField($model,'pay_method_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'delivery_method_id'); ?>
		<?php echo $form->textField($model,'delivery_method_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reward_status'); ?>
		<?php echo $form->textField($model,'reward_status',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->