<?php
/* @var $this PaymentController */
/* @var $model Payment */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'payment-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'sum'); ?>
		<?php echo $form->textField($model,'sum',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'sum'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model, 'status', array(
                0=>'Оплачен',
                1=>'Не оплачен',
            )); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'card4digits'); ?>
        <?php echo $form->textField($model,'card4digits',array('size'=>4,'maxlength'=>4)); ?>
        <?php echo $form->error($model,'card4digits'); ?>
    </div>

    <div class="row">
		<?php echo $form->labelEx($model,'project_id'); ?>
		<?php echo $form->textField($model,'project_id'); ?>
		<?php echo $form->error($model,'project_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reward_id'); ?>
		<?php echo $form->textField($model,'reward_id'); ?>
		<?php echo $form->error($model,'reward_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pay_method_id'); ?>
		<?php echo $form->textField($model,'pay_method_id'); ?>
		<?php echo $form->error($model,'pay_method_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'delivery_method_id'); ?>
		<?php echo $form->textField($model,'delivery_method_id'); ?>
		<?php echo $form->error($model,'delivery_method_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reward_status'); ?>
		<?php echo $form->textField($model,'reward_status',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'reward_status'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'comments'); ?>
        <?php echo $form->textArea($model,'comments',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'comments'); ?>
    </div>

    <div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->