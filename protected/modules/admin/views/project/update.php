<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
	'Projects'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Список проектов', 'url'=>array('index')),
	array('label'=>'Создать проект', 'url'=>array('create')),
	array('label'=>'Просмотр проекта', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управлять проектами', 'url'=>array('admin')),
);
?>

<h1>Update Project <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>