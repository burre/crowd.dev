<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
	'Projects'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Список проектов', 'url'=>array('index')),
	array('label'=>'Управлять проектами', 'url'=>array('admin')),
);
?>

<h1>Create Project</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>