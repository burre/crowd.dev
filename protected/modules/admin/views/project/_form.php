<?php
/* @var $this ProjectController */
/* @var $model Project */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'project-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'images'); ?>
        <p class="note">Размер: 423х330 пикселей или такое же соотношение сторон.</p>
        <?php $this->widget(
            'CMultiFileUpload',
            array(
                'name'        => 'files',
                'accept'      => 'jpg|png',
                'max'         => 3,
                'remove'      => Yii::t('ui', 'Remove'),
                //'denied'=>'', message that is displayed when a file type is not allowed
                //'duplicate'=>'', message that is displayed when a file appears twice
                'htmlOptions' => array('size' => 25),
            )
        ); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'for_whom'); ?>
		<?php echo $form->textArea($model,'for_whom',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'for_whom'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'destination'); ?>
		<?php echo $form->textArea($model,'destination',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'destination'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'target_sum'); ?>
		<?php echo $form->textField($model,'target_sum'); ?>
		<?php echo $form->error($model,'target_sum'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'start_date'); ?>
        <?php echo $form->dateField($model, 'start_date'); ?>
        <?php echo $form->error($model,'start_date'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'end_date'); ?>
        <?php echo $form->dateField($model,'end_date'); ?>
        <?php echo $form->error($model,'end_date'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model, 'status', array(
            'draft'=>'Черновик',
            'inprogress'=>'В процесе',
            'completed'=>'Завершен',
            'early'=>'Досрочно завершен',
            'archived'=>'В архиве',
            'deleted'=>'Удален',
        )); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comments'); ?>
		<?php echo $form->textArea($model,'comments',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'comments'); ?>
	</div>

    <?php if ($model->status == 'completed' || $model->status == 'early'): ?>
	<div class="row">
		<?php echo $form->labelEx($model,'videos'); ?>
        <p class="note">Один URL ролика с YouTube в каждой строке.</p>
		<?php echo $form->textArea($model,'videos',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'videos'); ?>
	</div>
    <?php endif ?>

    <div class="row">
        <?php echo $form->labelEx(ProjectRewards::model(),'reward_id'); ?>
        <?php echo $form->listBox(ProjectRewards::model(), 'reward_id',
            Reward::model()->getRewardsList(),
            array('empty'=> array(0=>'--- наград нет ---'),
                  'multiple'=>'multiple',
                  'style'=>'width:421px;',
                  'size'=>'10',
                  'options'=>ProjectRewards::model()->getRewardsAsOptions($model->id),
            )
        ); ?>
        <?php echo 'Ctrl+click снимает выделение с элемента'; ?>
        <?php echo $form->error($model,'reward_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx(ProjectPartners::model(),'partner_id'); ?>
        <?php echo $form->listBox(ProjectPartners::model(), 'partner_id',
            Partner::model()->getPartnersList(),
            array('empty'=> array(0=>'--- партнеров нет ---'),
                  'multiple'=>'multiple',
                  'style'=>'width:421px;',
                  'size'=>'10',
                  'options'=>ProjectPartners::model()->getPartnersAsOptions($model->id),
            )
        ); ?>
        <?php echo 'Ctrl+click снимает выделение с элемента'; ?>
        <?php echo $form->error($model,'partner_id'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->