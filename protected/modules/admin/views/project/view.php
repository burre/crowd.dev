<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
	'Projects'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'Список проектов', 'url'=>array('index')),
	array('label'=>'Создать проект', 'url'=>array('create')),
	array('label'=>'Изменить проект', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить проект', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управлять проектами', 'url'=>array('admin')),
);
?>

<h1>View Project #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'description',
//		'images',
		'for_whom',
		'destination',
		'target_sum',
        'start_date',
        'end_date',
        'status',
		'comments',
	),
)); ?>
