<?php
/* @var $this ProjectRewardsController */
/* @var $model ProjectRewards */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
	'Project Rewards'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProjectRewards', 'url'=>array('index')),
	array('label'=>'Create ProjectRewards', 'url'=>array('create')),
	array('label'=>'View ProjectRewards', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProjectRewards', 'url'=>array('admin')),
);
?>

<h1>Update ProjectRewards <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'projectList'=>$projectList, 'rewardList'=>$rewardList)); ?>