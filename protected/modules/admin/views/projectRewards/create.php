<?php
/* @var $this ProjectRewardsController */
/* @var $model ProjectRewards */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
	'Project Rewards'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProjectRewards', 'url'=>array('index')),
	array('label'=>'Manage ProjectRewards', 'url'=>array('admin')),
);
?>

<h1>Create ProjectRewards</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'projectList'=>$projectList, 'rewardList'=>$rewardList)); ?>