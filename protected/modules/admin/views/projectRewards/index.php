<?php
/* @var $this ProjectRewardsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
	'Project Rewards',
);

$this->menu=array(
	array('label'=>'Create ProjectRewards', 'url'=>array('create')),
	array('label'=>'Manage ProjectRewards', 'url'=>array('admin')),
);
?>

<h1>Project Rewards</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
