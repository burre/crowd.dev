<?php
/* @var $this ProjectRewardsController */
/* @var $model ProjectRewards */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
	'Project Rewards'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProjectRewards', 'url'=>array('index')),
	array('label'=>'Create ProjectRewards', 'url'=>array('create')),
	array('label'=>'Update ProjectRewards', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProjectRewards', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProjectRewards', 'url'=>array('admin')),
);
?>

<h1>View ProjectRewards #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'project_id',
		'reward_id',
	),
)); ?>
