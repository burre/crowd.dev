<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);

$this->menu=array(
    array('label'=>'Пользователи', 'url'=>array('/admin/user')),
    array('label'=>'Сообщения', 'url'=>array('/admin/message')),
    array('label'=>'Проекты', 'url'=>array('/admin/project')),
//    array('label'=>'Project partners', 'url'=>array('/admin/projectPartners')),
//    array('label'=>'Project rewards', 'url'=>array('/admin/projectRewards')),
    array('label'=>'Платежи', 'url'=>array('/admin/payment')),
//    array('label'=>'Методы доставки', 'url'=>array('/admin/deliveryMethod')),
//    array('label'=>'Методы оплаты', 'url'=>array('/admin/payMethod')),
    array('label'=>'Партнеры', 'url'=>array('/admin/partner')),
    array('label'=>'Награды', 'url'=>array('/admin/reward')),
);
?>
<h1>Интерфейс администратора</h1>
