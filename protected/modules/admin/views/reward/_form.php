<?php
/* @var $this RewardController */
/* @var $model Reward */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id'                   => 'reward-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype'=>'multipart/form-data'),
        )
    ); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name'); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'min_pay'); ?>
        <?php echo $form->textField($model, 'min_pay'); ?>
        <?php echo $form->error($model, 'min_pay'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'description'); ?>
        <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'description'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'images'); ?>
        <?php $this->widget(
            'CMultiFileUpload',
            array(
                'name'        => 'files',
                'accept'      => 'jpg|png',
                'max'         => 1,
                'remove'      => Yii::t('ui', 'Remove'),
                //'denied'=>'', message that is displayed when a file type is not allowed
                //'duplicate'=>'', message that is displayed when a file appears twice
                'htmlOptions' => array('size' => 25),
            )
        ); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->