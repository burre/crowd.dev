<?php
/* @var $this DeliveryMethodController */
/* @var $model DeliveryMethod */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
	'Delivery Methods'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DeliveryMethod', 'url'=>array('index')),
	array('label'=>'Create DeliveryMethod', 'url'=>array('create')),
	array('label'=>'View DeliveryMethod', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DeliveryMethod', 'url'=>array('admin')),
);
?>

<h1>Update DeliveryMethod <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>