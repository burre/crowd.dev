<?php
/* @var $this DeliveryMethodController */
/* @var $model DeliveryMethod */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
    'Delivery Methods'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List DeliveryMethod', 'url'=>array('index')),
	array('label'=>'Create DeliveryMethod', 'url'=>array('create')),
	array('label'=>'Update DeliveryMethod', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DeliveryMethod', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DeliveryMethod', 'url'=>array('admin')),
);
?>

<h1>View DeliveryMethod #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'description',
	),
)); ?>
