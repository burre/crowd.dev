<?php
/* @var $this DeliveryMethodController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
	'Delivery Methods',
);

$this->menu=array(
	array('label'=>'Create DeliveryMethod', 'url'=>array('create')),
	array('label'=>'Manage DeliveryMethod', 'url'=>array('admin')),
);
?>

<h1>Delivery Methods</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
