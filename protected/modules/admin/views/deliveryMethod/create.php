<?php
/* @var $this DeliveryMethodController */
/* @var $model DeliveryMethod */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
    'Delivery Methods'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DeliveryMethod', 'url'=>array('index')),
	array('label'=>'Manage DeliveryMethod', 'url'=>array('admin')),
);
?>

<h1>Create DeliveryMethod</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>