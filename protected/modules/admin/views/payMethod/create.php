<?php
/* @var $this PayMethodController */
/* @var $model PayMethod */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
    'Pay Methods'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PayMethod', 'url'=>array('index')),
	array('label'=>'Manage PayMethod', 'url'=>array('admin')),
);
?>

<h1>Create PayMethod</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>