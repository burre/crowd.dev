<?php
/* @var $this PayMethodController */
/* @var $model PayMethod */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
    'Pay Methods'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PayMethod', 'url'=>array('index')),
	array('label'=>'Create PayMethod', 'url'=>array('create')),
	array('label'=>'View PayMethod', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PayMethod', 'url'=>array('admin')),
);
?>

<h1>Update PayMethod <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>