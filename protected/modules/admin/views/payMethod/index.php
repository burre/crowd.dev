<?php
/* @var $this PayMethodController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
    'Pay Methods',
);

$this->menu=array(
	array('label'=>'Create PayMethod', 'url'=>array('create')),
	array('label'=>'Manage PayMethod', 'url'=>array('admin')),
);
?>

<h1>Pay Methods</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
