<?php
/* @var $this PayMethodController */
/* @var $model PayMethod */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
    'Pay Methods'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List PayMethod', 'url'=>array('index')),
	array('label'=>'Create PayMethod', 'url'=>array('create')),
	array('label'=>'Update PayMethod', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PayMethod', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PayMethod', 'url'=>array('admin')),
);
?>

<h1>View PayMethod #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'description',
	),
)); ?>
