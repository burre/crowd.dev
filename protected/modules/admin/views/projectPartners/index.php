<?php
/* @var $this ProjectPartnersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
    'Project Partners',
);

$this->menu=array(
	array('label'=>'Create ProjectPartners', 'url'=>array('create')),
	array('label'=>'Manage ProjectPartners', 'url'=>array('admin')),
);
?>

<h1>Project Partners</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
