<?php
/* @var $this ProjectPartnersController */
/* @var $model ProjectPartners */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
    'Project Partners'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProjectPartners', 'url'=>array('index')),
	array('label'=>'Create ProjectPartners', 'url'=>array('create')),
	array('label'=>'Update ProjectPartners', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProjectPartners', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProjectPartners', 'url'=>array('admin')),
);
?>

<h1>View ProjectPartners #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'project_id',
		'partner_id',
	),
)); ?>
