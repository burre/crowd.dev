<?php
/* @var $this ProjectPartnersController */
/* @var $model ProjectPartners */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
    'Project Partners'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProjectPartners', 'url'=>array('index')),
	array('label'=>'Manage ProjectPartners', 'url'=>array('admin')),
);
?>

<h1>Create ProjectPartners</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'projectList'=>$projectList, 'partnerList'=>$partnerList)); ?>