<?php
/* @var $this ProjectPartnersController */
/* @var $model ProjectPartners */

$this->breadcrumbs=array(
    'Admin' =>array('/admin'),
    'Project Partners'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProjectPartners', 'url'=>array('index')),
	array('label'=>'Create ProjectPartners', 'url'=>array('create')),
	array('label'=>'View ProjectPartners', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProjectPartners', 'url'=>array('admin')),
);
?>

<h1>Update ProjectPartners <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'projectList'=>$projectList, 'partnerList'=>$partnerList)); ?>