-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Апр 27 2015 г., 17:00
-- Версия сервера: 5.6.24-0ubuntu2
-- Версия PHP: 5.6.4-4ubuntu6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `crowd_dev`
--

-- --------------------------------------------------------

--
-- Структура таблицы `delivery_methods`
--

CREATE TABLE IF NOT EXISTS `delivery_methods` (
`id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `delivery_methods`
--

INSERT INTO `delivery_methods` (`id`, `title`, `description`) VALUES
(1, 'Укрпочта', ''),
(2, 'Новая почта', ''),
(3, 'Самовывоз', ''),
(4, 'Отказ', '');

-- --------------------------------------------------------

--
-- Структура таблицы `ha_logins`
--

CREATE TABLE IF NOT EXISTS `ha_logins` (
`id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `loginProvider` varchar(50) NOT NULL,
  `loginProviderIdentifier` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
`id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `full_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `institution` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `issue` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `user_id`, `full_name`, `institution`, `phone`, `email`, `issue`, `message`) VALUES
(1, NULL, 'Виктор', '1', '0631738381', 'burre@mail.ru', '1', 'Собственно вопрос.'),
(2, NULL, 'Виктор Бурре', 'Частное лицо', '0631738381', 'burre@mail.ru', 'Уточнить способ оплаты', 'Хочу оплатить с банковского счета.');

-- --------------------------------------------------------

--
-- Структура таблицы `partners`
--

CREATE TABLE IF NOT EXISTS `partners` (
`id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `logo` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `partners`
--

INSERT INTO `partners` (`id`, `name`, `description`, `logo`) VALUES
(1, 'Компания VIASAT', 'Компания VIASAT', 'upload/partners/1.jpg'),
(2, 'Аукцион eBay', 'Аукцион eBay\r\n', 'upload/partners/2.jpg'),
(3, 'Турагенція "Львів"', 'Турагенція "Львів"', 'upload/partners/3.jpg'),
(4, 'Збройні Сили України', 'Збройні Сили України', 'upload/partners/4.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
`id` int(11) NOT NULL,
  `sum` decimal(10,2) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `card4digits` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `reward_id` int(11) DEFAULT NULL,
  `pay_method_id` int(11) DEFAULT NULL,
  `delivery_method_id` int(11) DEFAULT NULL,
  `reward_status` enum('notsent','sent','processing','refused') COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `payments`
--

INSERT INTO `payments` (`id`, `sum`, `status`, `card4digits`, `project_id`, `user_id`, `reward_id`, `pay_method_id`, `delivery_method_id`, `reward_status`, `comments`) VALUES
(36, 100.00, 0, '2034', 9, 11, NULL, 1, 2, 'notsent', ''),
(37, 300.00, 0, '1231', 9, 12, NULL, 1, 2, 'notsent', ''),
(38, 10000.00, 0, '', 9, 11, NULL, 1, 2, 'notsent', ''),
(39, 5000.00, 0, '1212', 9, 13, NULL, 1, 2, 'notsent', ''),
(40, 5000.00, 0, '1234', 11, 14, NULL, 1, 2, 'notsent', ''),
(41, 40000.00, 0, '', 11, 15, NULL, 1, 2, 'notsent', ''),
(42, 50.00, 0, '1231', 9, 16, NULL, 1, 2, 'notsent', ''),
(43, 50.00, 0, '', 9, 15, NULL, 2, 3, 'notsent', ''),
(44, 50.00, 0, '', 9, 15, NULL, 2, 3, 'notsent', ''),
(45, 50.00, 0, '', 9, 15, NULL, 2, 3, 'notsent', ''),
(46, 50.00, 0, '1234', 9, 17, NULL, 3, 2, 'notsent', ''),
(47, 50.00, 0, '2342', 9, 18, NULL, 2, 3, 'notsent', ''),
(48, 50.00, 0, '', 9, 15, NULL, 2, 3, 'notsent', ''),
(49, 50.00, 0, '', 9, 15, NULL, 2, 3, 'notsent', ''),
(50, 50.00, 0, '', 9, 15, NULL, 2, 3, 'notsent', ''),
(51, 50.00, 0, '', 9, 15, NULL, 1, 4, 'notsent', '');

-- --------------------------------------------------------

--
-- Структура таблицы `pay_methods`
--

CREATE TABLE IF NOT EXISTS `pay_methods` (
`id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `pay_methods`
--

INSERT INTO `pay_methods` (`id`, `title`, `description`) VALUES
(1, 'Portmone.com', ''),
(2, 'Наличные', 'Только для Киева'),
(3, 'Наложенный платеж', '');

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
`id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `images` text COLLATE utf8_unicode_ci,
  `for_whom` text COLLATE utf8_unicode_ci,
  `destination` text COLLATE utf8_unicode_ci,
  `target_sum` decimal(10,2) NOT NULL DEFAULT '0.00',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` enum('draft','inprogress','completed','early','archived','deleted') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  `comments` text COLLATE utf8_unicode_ci,
  `videos` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `projects`
--

INSERT INTO `projects` (`id`, `title`, `description`, `images`, `for_whom`, `destination`, `target_sum`, `start_date`, `end_date`, `status`, `comments`, `videos`) VALUES
(8, 'Вундервафля № 1 для пехоты', '«Вундервафлей» в оружейных сообществах называют военную технику, претендующую на переворот всяческих основ, однако обычно не проработанную дальше идеи или эскиза. В отличие от редких, но реальных примеров всяческих вундерваффе, вундервафли поражают игнорированием основных принципов физики и гнетущим давлением придумавшего их сумрачного гения. Лучше всего это видно из примеров.\r\n\r\nКаждая вундервафля выглядит так, что Юнг и Фрейд, обнявшись, заплакали бы от радости.\r\n\r\nТем не менее некоторые вундервафли (летающие танки, подводные самолеты и пр.) таки существовали в реальности (военная мысль не ограничивается ничем, так как в уставе о ней не написано). Неплохая подборка подобных девайсов собрана в ВИБовской книге «Уникальная и парадоксальная военная техника».\r\n\r\nТакже настоящая вундервафля, которая ДНК микрокомпьютер.', '["upload\\/projects\\/item1.jpg","upload\\/projects\\/item2.jpg","upload\\/projects\\/item6.jpg"]', 'У Артура Кларка есть доставляющий рассказ о том, как на войне одна из сторон все время выставляет на поле битвы недоделанные вундервафли типа теплого лампового компьютера на миллион ламп. Пока недоделки устраняют, противники их преспокойненько заваливают звездолетами — тридцатьчетверками. С поправкой на технический уровень именно так обычно и выходит IRL.', 'Уберзольдаты\r\n\r\nБравый мандалор и его арсенал.\r\nЕсли чудо-арсенал достаточно миниатюрен, чтобы навесить его на человека, то мы получим уже юберзольднера, который гордится тяжёлой бронёй и арсеналом в рюкзаке (если есть у него вообще). Нет, родоначальником коспех из Doom не является, скорее — потомком. А звание «Родоначальник живых вундервафлей» гордо носит простой звёздный десантник в экзоскелете из одноименной нетленки Роберта Хайнлайна, из которой его вместе с его мехой попиздили все, кому не лень.\r\nТак же полный арсенал тру вундервафлей представлен в серии игр «Metal Gear», а последняя часть «Peace Walker» переплюнула даже саму себя.', 100000.00, '2014-09-26', '2014-10-25', 'inprogress', 'Без примечаний.', ''),
(9, 'Летающий танк А-40', 'А-40 («Летающий танк», «Крылья танка») — летательный аппарат, созданный Олегом Константиновичем Антоновым на базе танка Т-60. Изначально танк был предложен в качестве поддержки для партизан. Работа над планером велась с декабря 1941 года по февраль 1943 года, когда танк сняли с производства. Был изготовлен в апреле 1942 года в городе Тюмени[источник не указан 972 дня] в единственном экземпляре. Лётные испытания проходили под Москвой с 7 августа по 2 сентября 1942 года. Проводил их известный лётчик-планерист Сергей Анохин.', '["upload\\/projects\\/item3.png","upload\\/projects\\/item4.png","upload\\/projects\\/item5.jpg"]', 'Технические характеристики\r\n\r\nЭкипаж: 2 человека\r\nПассажировместимость: нет\r\nДлина планера: 12,0 м\r\nРазмах крыла: 18,0 м\r\nВысота: нет данных\r\nПлощадь коробки крыльев: 86 м2\r\nУдельная нагрузка на крыло: 90 кг/м2\r\nМасса облегчённого танка Т-60 с полезной нагрузкой: 5800 кг\r\nПолная полетная масса: 7800 кг\r\nМасса планера: 2000 кг\r\nДвигатели: ГАЗ-202 (70 л. с.) для движения по земле; в воздухе буксируется самолётом ТБ-3\r\nМощность: нет данных', 'Первый полёт «КТ» состоялся 2 сентября 1942 года. \r\n\r\nСамолётом-буксировщиком ТБ-3 с четырьмя усиленными (по 970 л. с.) моторами АМ-34РН командовал Павел Арсеньевич Еремеев, в прошлом конструктор пилотажных планеров. Планером управлял летчик-испытатель опытно-испытательного полигона воздушно-десантных войск Красной Армии Сергей Николаевич Анохин. Из-за большой массы и малой обтекаемости «КТ» буксировка велась на близкой к максимальной мощности двигателей ТБ-3 со скоростью 130 км/ч.[источник не указан 1759 дней] Несмотря на это, скорость подъёма аэропоезда оказалась недостаточной. Самолёт едва достиг высоты 40 м.', 500000.00, '2014-09-25', '2014-10-20', 'inprogress', '', ''),
(10, 'Вундервафля № 2 для десанта', '«Вундервафлей» в оружейных сообществах называют военную технику, претендующую на переворот всяческих основ, однако обычно не проработанную дальше идеи или эскиза. В отличие от редких, но реальных примеров всяческих вундерваффе, вундервафли поражают игнорированием основных принципов физики и гнетущим давлением придумавшего их сумрачного гения. Лучше всего это видно из примеров.', '["upload\\/projects\\/item2.jpg","upload\\/projects\\/item6.jpg"]', 'Каждая вундервафля выглядит так, что Юнг и Фрейд, обнявшись, заплакали бы от радости.\r\nТем не менее некоторые вундервафли (летающие танки, подводные самолеты и пр.) таки существовали в реальности (военная мысль не ограничивается ничем, так как в уставе о ней не написано). Неплохая подборка подобных девайсов собрана в ВИБовской книге «Уникальная и парадоксальная военная техника».', 'Если чудо-арсенал достаточно миниатюрен, чтобы навесить его на человека, то мы получим уже юберзольднера, который гордится тяжёлой бронёй и арсеналом в рюкзаке (если есть у него вообще). Нет, родоначальником коспех из Doom не является, скорее — потомком. А звание «Родоначальник живых вундервафлей» гордо носит простой звёздный десантник в экзоскелете из одноименной нетленки Роберта Хайнлайна, из которой его вместе с его мехой попиздили все, кому не лень.', 300000.00, '2014-09-24', '2014-10-15', 'inprogress', '', ''),
(11, 'Тестовый закрытый проект', 'Тестовый закрытый проект.\r\nТестовый закрытый проект.\r\nТестовый закрытый проект.\r\nТестовый закрытый проект.\r\nТестовый закрытый проект.\r\nТестовый закрытый проект.', '["upload\\/projects\\/3.jpg"]', '', '', 50000.00, '2014-09-05', '2014-11-02', 'early', '', '["http:\\/\\/www.youtube.com\\/embed\\/xoB2Q-wrFoI\\r","http:\\/\\/www.youtube.com\\/embed\\/xoB2Q-wrFoI"]'),
(12, 'Тестовый закрытый проект', 'Тестовый закрытый проект\r\nТестовый закрытый проект\r\nТестовый закрытый проект', '["upload\\/projects\\/2.jpg"]', '', '', 50000.00, '2014-09-03', '2014-10-03', 'completed', '', ''),
(13, 'Тестовый закрытый проект', 'Тестовый закрытый проект\r\nТестовый закрытый проект\r\nТестовый закрытый проект', '["upload\\/projects\\/1.jpg"]', '', '', 60000.00, '2014-09-03', '2014-10-03', 'completed', '', '["http:\\/\\/www.youtube.com\\/embed\\/xoB2Q-wrFoI"]'),
(14, 'Тестовый закрытый проект', 'Тестовый закрытый проект.\r\nТестовый закрытый проект.', '["upload\\/projects\\/1.jpg"]', '', '', 123000.00, '2014-09-18', '2014-10-02', 'completed', '', '["http:\\/\\/www.youtube.com\\/embed\\/xoB2Q-wrFoI\\r",""]'),
(15, 'Тестовый закрытый проект', 'Тестовый закрытый проект', '["upload\\/projects\\/2.jpg"]', '', '', 25000.00, '2014-09-04', '2014-10-01', 'early', '', '["http:\\/\\/www.youtube.com\\/embed\\/xoB2Q-wrFoI"]'),
(16, 'Тестовый закрытый проект', 'Тестовый закрытый проект\r\nТестовый закрытый проект', '["upload\\/projects\\/3.jpg"]', '', '', 300000.00, '2014-09-05', '2014-10-01', 'completed', '', ''),
(17, 'Тестовый закрытый проект', 'Тестовый закрытый проект', '["upload\\/projects\\/2.jpg"]', '', '', 220000.00, '2014-09-12', '2014-10-03', 'completed', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `projects_partners`
--

CREATE TABLE IF NOT EXISTS `projects_partners` (
`id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `partner_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `projects_partners`
--

INSERT INTO `projects_partners` (`id`, `project_id`, `partner_id`) VALUES
(6, 8, 1),
(7, 8, 2),
(14, 9, 1),
(15, 9, 2),
(17, 9, 4),
(18, 11, 3),
(19, 11, 4),
(20, 12, 1),
(21, 12, 3),
(22, 12, 4),
(23, 13, 2),
(24, 13, 3),
(25, 14, 1),
(26, 14, 4),
(27, 15, 1),
(28, 15, 3),
(29, 16, 1),
(30, 16, 2),
(31, 16, 3),
(32, 17, 1),
(33, 17, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `projects_rewards`
--

CREATE TABLE IF NOT EXISTS `projects_rewards` (
`id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `reward_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `projects_rewards`
--

INSERT INTO `projects_rewards` (`id`, `project_id`, `reward_id`) VALUES
(20, 9, 20),
(22, 9, 22),
(24, 9, 21),
(32, 11, 20),
(33, 11, 21),
(34, 11, 22),
(35, 13, 20),
(36, 13, 21),
(39, 14, 20),
(40, 14, 21),
(41, 15, 20),
(42, 15, 22),
(43, 16, 20),
(44, 16, 21),
(45, 16, 22),
(46, 17, 20),
(47, 17, 21),
(48, 17, 22);

-- --------------------------------------------------------

--
-- Структура таблицы `rewards`
--

CREATE TABLE IF NOT EXISTS `rewards` (
`id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_pay` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `images` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `rewards`
--

INSERT INTO `rewards` (`id`, `name`, `min_pay`, `description`, `images`) VALUES
(20, 'Cиликоновый браслет “Допомогаю нашим”', 50, 'Cиликоновый браслет “Допомогаю нашим”\r\n', 'upload/rewards/1.png'),
(21, 'Футбольный мяч с автографами игроков ФК “Динамо” ', 100, 'Футбольный мяч с автографами игроков ФК “Динамо” Киев', 'upload/rewards/2.png'),
(22, 'Игровая форма ФК “Динамо” Киев с автографами ветер', 300, 'Игровая форма ФК “Динамо” Киев с автографами ветеранов клуба', 'upload/rewards/3.png');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `first_name` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `city`, `address`, `phone`, `comments`) VALUES
(11, 'Виктор', 'Бурре', 'burre@mail.ru', 'Чернигов', '', '', NULL),
(12, 'Иван', 'Петров', 'petrov@mail.ru', '', '', '', NULL),
(13, 'Иванов', 'на', 'ibanov@ukr.net', 'Донецк', '12309', '', NULL),
(14, 'Пупкин', 'pupkin@mail.ru', 'pupkin@mail.ru', 'Москва', '111111', '', NULL),
(15, '', '', '', '', '', '', NULL),
(16, 'Бурак', 'В', 'burak@mail.ru', 'Чернигов', '123123', '123123123123', NULL),
(17, 'Бурак', 'asd@mail.ru', 'asd@mail.ru', 'фывафыва', '123123', '123123123123', NULL),
(18, 'asfdafsdf', 'asdfa@mail.ru', 'asdfa@mail.ru', 'asdfasdf', '234234', '23523452345', NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `delivery_methods`
--
ALTER TABLE `delivery_methods`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ha_logins`
--
ALTER TABLE `ha_logins`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `loginProvider_2` (`loginProvider`,`loginProviderIdentifier`), ADD KEY `loginProvider` (`loginProvider`), ADD KEY `loginProviderIdentifier` (`loginProviderIdentifier`), ADD KEY `userId` (`userId`), ADD KEY `id` (`id`);

--
-- Индексы таблицы `messages`
--
ALTER TABLE `messages`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `partners`
--
ALTER TABLE `partners`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `payments`
--
ALTER TABLE `payments`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_payments_1_idx` (`project_id`), ADD KEY `fk_payments_2_idx` (`user_id`), ADD KEY `fk_payments_3_idx` (`reward_id`), ADD KEY `fk_payments_4_idx` (`pay_method_id`), ADD KEY `fk_payments_5_idx` (`delivery_method_id`);

--
-- Индексы таблицы `pay_methods`
--
ALTER TABLE `pay_methods`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `projects`
--
ALTER TABLE `projects`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_STATUS` (`status`);

--
-- Индексы таблицы `projects_partners`
--
ALTER TABLE `projects_partners`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_projects_partners_1_idx` (`project_id`), ADD KEY `fk_projects_partners_2_idx` (`partner_id`);

--
-- Индексы таблицы `projects_rewards`
--
ALTER TABLE `projects_rewards`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_projects_rewards_1_idx` (`project_id`), ADD KEY `fk_projects_rewards_2_idx` (`reward_id`);

--
-- Индексы таблицы `rewards`
--
ALTER TABLE `rewards`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `delivery_methods`
--
ALTER TABLE `delivery_methods`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `ha_logins`
--
ALTER TABLE `ha_logins`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `messages`
--
ALTER TABLE `messages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `partners`
--
ALTER TABLE `partners`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `payments`
--
ALTER TABLE `payments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT для таблицы `pay_methods`
--
ALTER TABLE `pay_methods`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `projects`
--
ALTER TABLE `projects`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблицы `projects_partners`
--
ALTER TABLE `projects_partners`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT для таблицы `projects_rewards`
--
ALTER TABLE `projects_rewards`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT для таблицы `rewards`
--
ALTER TABLE `rewards`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `payments`
--
ALTER TABLE `payments`
ADD CONSTRAINT `fk_payments_delivery_method` FOREIGN KEY (`delivery_method_id`) REFERENCES `delivery_methods` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_payments_pay_method` FOREIGN KEY (`pay_method_id`) REFERENCES `pay_methods` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_payments_project` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_payments_reward` FOREIGN KEY (`reward_id`) REFERENCES `rewards` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_payments_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `projects_partners`
--
ALTER TABLE `projects_partners`
ADD CONSTRAINT `fk_projects_partners_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_projects_partners_2` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `projects_rewards`
--
ALTER TABLE `projects_rewards`
ADD CONSTRAINT `fk_projects_rewards_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_projects_rewards_2` FOREIGN KEY (`reward_id`) REFERENCES `rewards` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
