<?php
/**
 * Project: HelpingOur (crowd.dev)
 * File:    _donate_form.php
 * Date:    26.09.14 @ 12:04
 *
 * Author:  Victor Burak <victor.burre@gmail.com>
 */

/* @var $project Project */
/* @var $donate DonateForm */
/* @var $form CActiveForm */
?>
<script>
    $('document').ready(function () {
        var pay2delivery = {
            pay_portmone: ['ship_ukrpost', 'ship_newpost', 'ship_himself', 'ship_cancel'],
            pay_cash: ['ship_himself', 'ship_cancel'],
            pay_on_delivery: ['ship_ukrpost', 'ship_newpost', 'ship_cancel']
        };

        var delivery2pay = {
            ship_ukrpost: ['pay_portmone', 'pay_on_delivery'],
            ship_newpost: ['pay_portmone', 'pay_on_delivery'],
            ship_himself: ['pay_portmone', 'pay_cash'],
            ship_cancel: ['pay_portmone', 'pay_cash', 'pay_on_delivery']
        };

        // payment mouse over
        $('li[id^=pay_]').mouseenter(function (e) {
            if ($('[id^=pay_].selected').length == 0) {
                $('[id^=pay_]').removeClass('enabled');
                $(this).addClass('enabled');

                $('[id^=ship_]').removeClass('enabled');
                $('[id^=ship_]').addClass('disabled');
                pay2delivery[this.id].forEach(function (ship) {
                    $('#' + ship).removeClass('disabled');
                    $('#' + ship).addClass('enabled');
                }, this);
            }
        });

        $('li[id^=pay_]').mouseleave(function (e) {
            if ($('[id^=pay_].selected').length == 0) {
                $('[id^=pay_]').removeClass('enabled');
                $('[id^=ship_]').removeClass('enabled');
            }
        });

        // shipment mouse over
        $('li[id^=ship_]').mouseenter(function (e) {
            $('[id^=ship_]').removeClass('enabled');
            // if selected pay method
            if ($('[id^=pay_].selected').length > 0) {
                if ($(this).data('enabled')) {
                    $(this).addClass('enabled');
                } else {
                    $(this).addClass('disabled');
                }
            } else {
                $(this).addClass('enabled');

                // liht available pay methods
                $('[id^=pay_]').removeClass('enabled');
                $('[id^=pay_]').addClass('disabled');
                delivery2pay[this.id].forEach(function(pay) {
                    $('#'+pay).removeClass('disabled');
                    $('#'+pay).addClass('enabled');
                }, this);

            }

            if (!$(this).data('enabled')) {
            }
        });

        // paymethods trigger
        $('li[id^=pay_]').click(function (e) {
            $('[id^=pay_]').removeClass('enabled disabled selected');
            $('[id^=pay_]').addClass('disabled');
            $('[id^=ship_]').removeClass('enabled disabled selected');
            $('[id^=ship_]').addClass('disabled');
            $(this).removeClass('disabled');
            $(this).addClass('selected');
            $('[id^=ship_]').data('enabled', false);
            pay2delivery[this.id].forEach(function(ship) {
                $('#'+ship).data('enabled', true);
                $('#'+ship).removeClass('disabled');
                $('#'+ship).addClass('enabled');
            }, this);
            $('#DonateForm_paymethod').val(this.id.replace(/pay_/g, ''));
            $('#DonateForm_shipmethod').val('');
            $('.fields-wrapper').fadeOut();
        });

        // shipment methods trigger
        $('li[id^=ship_]').click(function (e) {
            if ($(this).data('enabled')) {
                $('[id^=ship_]').removeClass('enabled disabled selected');
                $('[id^=ship_]').addClass('disabled');
                $(this).removeClass('disabled');
                $(this).addClass('selected');
                $('#DonateForm_shipmethod').val(this.id.replace(/ship_/g, ''));

                if(this.id == 'ship_ukrpost' || this.id == 'ship_newpost') {
                    $('.fields-wrapper').fadeIn();
                } else {
                    $('.fields-wrapper').fadeOut();
                }
            }
        });

    });
</script>

<div class="popup">
    <a href="#" class="close"></a>
    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id'                     => get_class($donate),
            'action'                 => Yii::app()->createAbsoluteUrl('project/donate'),
            'enableClientValidation' => true,
            'clientOptions'          => array(
                'validateOnSubmit' => true,
            ),
            'htmlOptions'            => array(
                'class' => 'shipment-form',
            ),
        )
    ); ?>

    <div class="row">
        <?php echo $form->labelEx($donate, 'sum', array('class' => 'first_label')); ?>
        <div class="right">
            <?php echo $form->textField($donate, 'sum'); ?> грн.
        </div>
    </div>

    <div class="title">Виберіть спосіб оплати:</div>
    <ul class="payment">
        <li id="pay_portmone">
            <img src="/images/ico/portmone.png"/>

            <div>Переказ з карток Visa і Master Cart через сервіс portmone.com</div>
        </li>
        <li id="pay_cash">
            <img src="/images/ico/cash.png"/>

            <div>
                Оплата готівкою<br/>
                (доступно тільки для Киева)
            </div>
        </li>
        <li id="pay_on_delivery">
            <img src="/images/ico/post.png"/>

            <div>Післяплата</div>
        </li>
    </ul>
    <div class="title">Виберіть спосіб доставки:</div>
    <ul class="shipment">
        <li id="ship_ukrpost">
            <img src="/images/ico/ukr_post.png"/>

            <div>Укрпошта</div>
        </li>
        <li id="ship_newpost">
            <img src="/images/ico/nv.png"/>

            <div>Нова пошта</div>
        </li>
        <li id="ship_himself">
            <img src="/images/ico/samovivoz.png"/>

            <div>Самовивіз</div>
        </li>
        <li id="ship_cancel">
            <img src="/images/ico/cancel.png"/>

            <div>Відмовитись від доставки подарунка</div>
        </li>
    </ul>

    <div class="fields-wrapper">
    <div class="title">Форма для відправки подарункка:</div>
    <div class="row">
        <?php echo $form->labelEx($donate, 'name'); ?>
        <div>
            <?php echo $form->textField($donate, 'name'); ?>
        </div>
    </div>
    <div class="row">
        <?php echo $form->labelEx($donate, 'city'); ?>
        <div>
            <?php echo $form->textField($donate, 'city'); ?>
        </div>
    </div>
    <div class="row">
        <?php echo $form->labelEx($donate, 'address'); ?>
        <div>
            <?php echo $form->textField($donate, 'address'); ?>
        </div>
    </div>
    <div class="row">
        <?php echo $form->labelEx($donate, 'card4digits'); ?>
        <div>
            <?php echo $form->textField($donate, 'card4digits'); ?>
        </div>
    </div>
    <div class="row">
        <?php echo $form->labelEx($donate, 'email'); ?>
        <div>
            <?php echo $form->textField($donate, 'email'); ?>
        </div>
    </div>
    <div class="row">
        <?php echo $form->labelEx($donate, 'phone'); ?>
        <div>
            <?php echo $form->textField($donate, 'phone'); ?>
        </div>
    </div>
    <div class="row">
        <?php echo $form->labelEx($donate, 'comments'); ?>
        <div>
            <?php echo $form->textArea($donate, 'comments'); ?>
        </div>
    </div>
    </div> <!-- /fields-wrapper -->
    <div class="tar">
        <?php echo $form->hiddenField($donate, 'project_id'); ?><br>
        <?php echo $form->hiddenField($donate, 'paymethod'); ?><br>
        <?php echo $form->hiddenField($donate, 'shipmethod'); ?><br>
        <?php echo CHtml::ajaxSubmitButton(
            'Відправити',
            Yii::app()->createAbsoluteUrl('project/donate'),
            array( // ajax options
                'method'  => 'POST',
                'dataType'=> 'JSON',
                'success' => 'function(data){
                    if (data.result == "success" && data.redirect == "portmone") {
                        $(".popup").hide();
                        $("#PortmoneForm>#bill_amount").val(data.sum);
                        $("#PortmoneForm").submit();
                    } else if (data.result == "success" && data.redirect == "cash") {
                        console.log("cash - " + $("#msg2").html());
                        $(".popup").hide();
                        $(".flash_msg div#msg").html($("#msg2").html());
                        $(".flash_msg").show();
                    } else if (data.result == "error") {
                    } else {
                        $(".popup").hide();
                        $(".flash_msg").show();
                    }
                }'
            ),
            array( // html options
                   'type' => 'submit'
            )
        ); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>
<?php echo CHtml::beginForm('https://www.portmone.com.ua/r3/ru/services/card-to-card/index/id/8505',
    'post',  array('id'=>'PortmoneForm')); ?><br>
<?php echo CHtml::hiddenField('bill_amount', ''); ?><br>
<?php echo CHtml::hiddenField('description', '4731185602796788'); ?><br>
<?php echo CHtml::hiddenField('success_url', Yii::app()->request->getUrl()); ?><br>
<?php echo CHtml::endForm(); ?>
