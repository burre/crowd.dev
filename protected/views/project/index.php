<?php
/* @var $this ProjectController */
/* @var $project Project */
?>
<script>
    $(document).ready(function() {
        $("div.project div.photo, div.project div.button_wrapper").mouseenter(function () {
            var donates = $(this).parent().find("div.donates:has(div.donate)");
            var donatesH = donates.outerHeight();
                donates.css("top", -1 * donatesH);
                donates.fadeIn();
        });
        $("div.project div.photo, div.project div.button_wrapper").mouseleave(function () {
            var donates = $(this).parent().find("div.donates");
            donates.fadeOut();
        });
    });
</script>
<div class="body">
    <?php $i = 1; ?>
    <?php foreach ($projects as $project): ?>
    <?php $images = json_decode($project->images); ?>
    <?php echo ($i % 2) ? '<div class="clear projects">' : ''; ?>
        <div class="project <?php echo ($project->status=='completed' || $project->status=='early') ? 'completed' : ''; ?>">
            <h1><a href="<?php echo $this->createUrl('project/view', ['id'=>$project->id]) ?>"><?php echo $project->title; ?></a></h1>
            <div class="photo" >
                <a href="<?php echo $this->createUrl('project/view', ['id'=>$project->id]) ?>">
                    <img src="/<?php echo $images[0]; ?>" />
                </a>
            </div>
            <div class="description">
                <?php echo $project->description; ?>
            </div>
            <div class="clear">
                <div class="progress_wrapper">
                    <div class="progress_bar">
                        <div class="progress" style="width: <?php echo $project->getCollectedPercentage(); ?>%"></div>
                        <div class="progress_percent"><?php echo $project->getCollectedPercentage(); ?>%</div>
                    </div>
                    <div class="info">
                        <div class="collected">
                            <div class="info_title"><?php echo (int)$project->getCollectedSum(); ?></div>
                            <div>грн. зібрано</div>
                        </div>
                        <div class="members">
                            <div class="info_title"><?php echo $project->getMemberCount(); ?></div>
                            <div>учасники</div>
                        </div>
                        <div class="budget">
                            <div class="info_title"><?php echo (int)$project->target_sum; ?></div>
                            <div>грн. потрібно</div>
                        </div>
                    </div>
                </div>
                <div class="button_wrapper">
                    <div class="donates">
                        <?php if ($project->status=='inprogress'): ?>
                            <?php $rewards = $project->getAllRewards(); ?>
                            <?php $k = 0; ?>
                            <?php foreach ($rewards as $r): ?>
                                <div class="donate <?php echo (++$k % 2) == 1 ? 'odd' : ''; ?>">
                                    <div class="title">от <?php echo $r->min_pay; ?> грн.</div>
                                    <div class="photo">
                                        <img src="/<?php echo $r->images; ?>"/>
                                    </div>
                                    <div class="gift_title">
                                        <?php echo $r->description; ?>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        <?php elseif ($project->status=='early'): ?>
                            <div class="donate">
                                <h3>Проект завершено достроково</h3>
                                Зібрано на сайті: <?php echo (int)$project->getCollectedSum(); ?> (<?php echo $project->getCollectedPercentage(); ?>%)<br>
                                Отримано з інших джерел: <?php echo (int)$project->target_sum - (int)$project->getCollectedSum() ?> (<?php echo 100 - $project->getCollectedPercentage(); ?>%)<br><br>
                            </div>
                        <?php endif ?>
                    </div>
                    <a href="<?php echo $this->createUrl('project/view', ['id'=>$project->id]); ?>" class="button"><?php echo ($project->status=='inprogress') ? 'підтримати' : 'завершено'; ?></a>
                </div>
            </div>
        </div>
    <?php echo ($i % 2 == 0) ? '</div>' : ''; ?>
    <?php $i++; ?>
    <?php endforeach ?>
</div>

<?php if ($pages): ?>
<div class="pager">
    <?php $this->widget('CLinkPager', array(
            'header' => '',
            'nextPageLabel' => 'далее &raquo;',
            'prevPageLabel' => '&laquo; назад',
            'pages' => $pages,
        )); ?>
</div>

<?php endif ?>
<?php if ($pages===null): ?>
<div class="pager"><?php echo CHtml::link('переглянути всі проекти',array('project/index')); ?></div>
<?php endif ?>