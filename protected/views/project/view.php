<?php
/* @var $this ProjectController */
/* @var $project Project */
/* @var $donaate DonateForm */

$project->getImages();
?>
<script>
    $('document').ready(function () {
        // open donate form with specified sum
        $('a[id^=donate-]').click(function (e) {
            $('#DonateForm_sum').val(this.id.replace(/[^0-9]/g, ''));
            $('.mask').show();
            $('.popup').show(200);
        });

        // change main photo
        $('.photo_thumbs>img').click(function () {
            $('.photo_main>img').attr('src', $(this).attr('src'));
            $('.photo_thumbs>img').addClass('dim');
            $(this).removeClass('dim');
        });

        $('.photo_thumbs>img:first').removeClass('dim');
    });
</script>

<div class="flash_msg">
    <a href="#" class="close"></a>
    <div id="msg">Дякуємо за вашу участь.<br/>
    Ми вийдемо на зв’язок з вами найближчим часом.
    </div>
</div>
<div id="msg2" style="display:none">Дякуємо за вашу участь.<br/>
    Наша адреса: м.Київ, вул. Жулянська, 68.
</div>


<div class="body project_page">
    <h1><?php echo $project->title; ?></h1>

    <div class="clear column_2">
        <div class="col_1">
            <div class="photos">
                <div class="photo_main">
                    <img src="/<?php echo $project->images[0]; ?>"/>
                </div>
                <div class="photo_thumbs">
                    <?php foreach ($project->images as $image): ?>
                        <img src="/<?php echo $image; ?>" class="dim"/>
                    <?php endforeach ?>
                </div>
            </div>
            <div class="progress_wrapper <?php echo ($project->status=='completed' || $project->status=='early') ? 'completed' : ''; ?>">
                <div class="progress_bar">
                    <div class="progress" style="width: <?php echo $project->getCollectedPercentage(); ?>%"></div>
                    <div class="progress_percent"><?php echo $project->getCollectedPercentage(); ?>%</div>
                </div>
                <div class="info">
                    <div class="collected">
                        <div class="info_title"><?php echo (int)$project->getCollectedSum(); ?></div>
                        <div>грн. зібрано</div>
                    </div>
                    <div class="members">
                        <div class="info_title"><?php echo $project->getMemberCount(); ?></div>
                        <div>учасники</div>
                    </div>
                    <div class="budget">
                        <div class="info_title"><?php echo (int)$project->target_sum; ?></div>
                        <div>грн. потрібно</div>
                    </div>
                </div>
            </div>
            <h1>Партнери:</h1>
            <?php $partners = $project->getAllPartners(); ?>
            <ul class="partners">
                <?php foreach ($partners as $partner): ?>
                    <li>
                        <a href="#">
                            <img src="/<?php echo $partner->logo; ?>"/>
                        </a>
                    </li>
                <?php endforeach ?>
            </ul>
        </div>
        <div class="col_2">
            <h2>Загальна інформація:</h2>
            <div><?php echo $project->description; ?></div>
            <br/>

            <h2>Для кого:</h2>
            <div><?php echo $project->for_whom; ?></div>
            <br/>

            <h2>Куди:</h2>
            <div><?php echo $project->destination; ?></div>
            <br/>

            <?php if ($project->status=='inprogress'): ?>
            <h2>Подяка учасникам:</h2>
            <?php $rewards = $project->getAllRewards(); ?>
            <?php $i = 0; ?>
            <?php foreach ($rewards as $r): ?>
                <div class="donate <?php echo (++$i % 2) == 1 ? 'odd' : ''; ?>">
                    <div class="title">от <?php echo $r->min_pay?> грн.</div>
                    <div class="photo">
                        <img src="/<?php echo $r->images; ?>"/>
                    </div>
                    <div class="gift_title">
                        <?php echo $r->description; ?>
                    </div>
                    <a href="#" id="donate-<?php echo $r->min_pay; ?>" class="button">підтримати</a>
                </div>
            <?php endforeach ?>
            <?php endif ?>
        </div>
    </div>
</div>

<?php $this->renderPartial('_donate_form', array('project' => $project, 'donate' => $donate,)); ?>
