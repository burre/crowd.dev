<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<div class="mask"></div>

<?php $this->renderPartial('//layouts/_header') ?>

<?php echo $content; ?>

<?php $this->renderPartial('//layouts/_footer') ?>

</body>
</html>