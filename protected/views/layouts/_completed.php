<?php
/**
 * Project: HelpingOur (crowd.dev)
 * File:    _completed.php
 * Date:    09.09.14 @ 16:18
 *
 * Author:  Victor Burak <victor.burre@gmail.com>
 */
?>
<div class="completed_projects">
    <div class="body">
        <h1>Завершені проекти</h1>

        <div class="slideshow_nav">
            <a href=# id="slide_prev">Prev</a>

            <div id="slide_caption">
                <span class="slideNum"></span>
                із
                <span class="slideCount"></span>
            </div>
            <a href=# id="slide_next">Next</a>
        </div>
        <?php
            $projects = Project::getAllByStatus(['completed', 'early']);
            $i = 0;
        ?>
        <div class="projects_slider">
        <?php foreach ($projects as $project): ?>
        <?php if ($i%4 == 0): ?>
            <div class="slide">
        <?php endif ?>
                <div class="project completed">
                    <h3><?php echo $project->title; ?></h3>
                    <?php $project->getImages(); ?>
                    <div class="photo">
                        <img src="/<?php echo $project->images[0];?>"/>
                        <img src="/images/ico/done.png" class="sticker"/>
                        <a href="<?php echo $this->createUrl('report/view', ['id'=>$project->id]) ?>" class="button">отчет</a>
                    </div>
                    <div class="description">
                        <?php echo $project->trim('description', 230); ?>
                    </div>
                    <div class="progress_wrapper">
                        <div class="progress_bar">
                            <div class="progress" style="width: <?php echo $project->getCollectedPercentage(); ?>%"></div>
                            <div class="progress_percent"><?php echo $project->getCollectedPercentage(); ?>%</div>
                        </div>
                        <div class="info">
                            <div class="collected">
                                <div class="info_title"><?php echo (int)$project->getCollectedSum(); ?></div>
                                <div>зібрано</div>
                            </div>
                            <div class="members">
                                <div class="info_title"><?php echo $project->getMemberCount(); ?></div>
                                <div>участника</div>
                            </div>
                            <div class="budget">
                                <div class="info_title"><?php echo (int)$project->target_sum; ?></div>
                                <div>грн. потрібно</div>
                            </div>
                        </div>
                    </div>
                </div>
        <?php $i++; ?>
        <?php if ($i%4 == 0 || $i == count($projects)): ?>
            </div> <!-- .slide -->
        <?php endif ?>
        <?php endforeach ?>
        </div>
    </div>
</div>

<script>
//    $(document).ready(function () {
//        var d = $('div.slide > div.completed > div.description');
//        var maxH = 0;
//
//        for (var i = 0; i < d.length; i++) {
//            h = $(d[0]).height();
//            if (h > maxH) maxH = h;
//        }
//
//        $('div.slide > div.completed > div.description').height(maxH);
//    });
</script>