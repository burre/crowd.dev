<?php
/**
 * Project: HelpingOur (crowd.dev)
 * File:    _header.php
 * Date:    09.09.14 @ 16:53
 *
 * Author:  Victor Burak <victor.burre@gmail.com>
 */
?>
<div class="header">
    <div class="body">
        <div class="clear">
            <a href="/" class="logo" >
                <img src="/images/logo.png" />
            </a>
            <ul class="menu">
                <li>
                    <a href="/about">про проект</a>
                </li>
                <li>
                    <a href="/project">в роботі</a>
                </li>
                <li>
                    <a href="/report">звіти</a>
                </li>
                <li>
                    <a href="/join">приєднатися до проекту</a>
                </li>
            </ul>
            <ul class="lang">
                <li>
                    <a href="#" class="ru" ></a>
                </li>
                <li>
                    <a href="#" class="ua" ></a>
                </li>
            </ul>
            <ul class="socials">
                <li>
                    <a href="#" class="vk" ></a>
                </li>
                <li>
                    <a href="#" class="tw" ></a>
                </li>
                <li>
                    <a href="#" class="email" ></a>
                </li>
                <li>
                    <a href="#" class="fb" ></a>
                </li>
                <li>
                    <a href="#" class="yt" ></a>
                </li>
            </ul>
        </div>
    </div>
</div>
