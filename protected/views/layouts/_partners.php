<?php
/**
 * Project: HelpingOur (crowd.dev)
 * File:    _partners.php
 * Date:    09.09.14 @ 16:11
 *
 * Author:  Victor Burak <victor.burre@gmail.com>
 */
?>
<div class="body">
    <h1>Партнери</h1>
    <div class="clear tac">
        <div class="partners">
            <?php
                $criteria = new CDbCriteria();
                $criteria->limit = 6;
                $partners = Partner::model()->findAll($criteria);
            ?>
            <?php foreach ($partners as $partner): ?>
                <a href="#">
                    <img src="/<?php echo $partner->logo; ?>" />
                </a>
            <?php endforeach ?>
        </div>
    </div>
</div>
