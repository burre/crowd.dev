<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

    <div class="content">
        <?php echo $content; ?>

        <?php $this->renderPartial('//layouts/_completed') ?>

        <?php $this->renderPartial('//layouts/_partners') ?>
    </div>

<?php $this->endContent(); ?>