<?php
/* @var $this ReportController */
/* @var $project Project */

$project->getImages();
$project->getVideos();
?>
<script>
    $('document').ready(function () {
        // change main photo
        $('.media>.overlay').click(function () {
            $('.media_active').html($(this).parent().html());
            $('.media').addClass('dim');
            $(this).parent().removeClass('dim');
        });

        $('.medialist>div:first').removeClass('dim');
    });
</script>

<div class="body report_page">
    <div class="clear column_2">
        <div class="col_1">
            <h1><?php echo $project->title; ?></h1>
            <div class="media_active">
                <?php if ($project->videos): ?>
                    <iframe width="582" height="437" src="<?php echo $project->videos[0]; ?>" frameborder="0"></iframe>
                <?php else: ?>
                    <img src="/<?php echo $project->images[0]; ?>" />
                <?php endif ?>
            </div>
            <div class="medialist">
                <?php if ($project->videos): ?>
                    <?php foreach ($project->videos as $video): ?>
                        <div class="media dim">
                            <div class="overlay"></div>
                            <iframe src="<?php echo $video; ?>" frameborder="0"></iframe>
                        </div>
                    <?php endforeach ?>
                <?php endif ?>
                <?php if ($project->images): ?>
                    <?php foreach ($project->images as $image): ?>
                        <div class="media dim">
                            <div class="overlay"></div>
                            <img src="/<?php echo $image; ?>" />
                        </div>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
        </div>
        <div class="col_2">
            <h1>Проект завершено <?php echo ($project->status == 'early') ? 'достроково' : ''; ?></h1>
            <div class="tac">
                <div class="progress_wrapper <?php echo ($project->status=='completed' || $project->status=='early') ? 'completed' : ''; ?>">
                    <div class="progress_bar">
                        <div class="progress" style="width: <?php echo $project->getCollectedPercentage(); ?>%"></div>
                        <div class="progress_percent"><?php echo $project->getCollectedPercentage(); ?>%</div>
                    </div>
                    <div class="info">
                        <div class="collected">
                            <div class="info_title"><?php echo (int)$project->getCollectedSum(); ?></div>
                            <div>грн. зібрано</div>
                        </div>
                        <div class="members">
                            <div class="info_title"><?php echo $project->getMemberCount(); ?></div>
                            <div>учасники</div>
                        </div>
                        <div class="budget">
                            <div class="info_title"><?php echo (int)$project->target_sum; ?></div>
                            <div>грн. потрібно</div>
                        </div>
                    </div>
                </div>
            </div>
            <h2>Загальна інформація:</h2>
            <div><?php echo $project->description; ?></div>
            <br/>

            <h2>Для кого:</h2>
            <div><?php echo $project->for_whom; ?></div>
            <br/>

            <h2>Куди:</h2>
            <div><?php echo $project->destination; ?></div>
            <br/>

            <h2>Партнеры:</h2>
            <?php $partners = $project->getAllPartners(); ?>
            <ul class="partners">
                <?php foreach ($partners as $partner): ?>
                    <li>
                        <a href="#">
                            <img src="/<?php echo $partner->logo; ?>"/>
                        </a>
                    </li>
                <?php endforeach ?>
            </ul>
        </div>
    </div>
</div>
