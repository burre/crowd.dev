<?php
/**
 * Project: HelpingOur (crowd.dev)
 * File:    about.php
 * Date:    08.09.14 @ 14:54
 *
 * Author:  Victor Burak <victor.burre@gmail.com>
 */
/** @var $model Message */
?>
<div class="body about">
    <div class="clear column_2">
        <div class="col_1">
            <h1>О проекте </h1>

            <h2>Общая информация:</h2>

            <div>Коллиматорный прицел - это оптическая система на основе пучка параллельных лучей. Параллельность их
                расположения обеспечивает неискаженное наблюдение цели в пределах видимости, поскольку для глаза стрелка
                рефлектор представляет собой плоско-параллельную пластину. Прицеливание может осуществляться как одним,
                так и двумя глазами, что создает для стрелка ничем не ограниченное поле зрения. Прицеливание может
                осуществляться как одним, так и двумя глазами, что создает для стрелка ничем не ограниченное поле
                зрения. Прицеливание может осуществляться как одним, так и двумя глазами, что создает для стрелка ничем
                не ограниченное поле зрения.
            </div>
            <br/>

            <h2>Для кого:</h2>

            <div>Прицеливание может осуществляться как одним, так и двумя глазами, что создает для стрелка ничем не
                ограниченное поле зрения. Прицеливание может осуществляться как одним, так и двумя глазами, что создает
                для стрелка ничем не ограниченное поле зрения. Прицеливание может осуществляться как одним, так и двумя
                глазами, что создает для стрелка ничем не ограниченное поле зрения.
            </div>
            <br/>

            <h2>Куда:</h2>

            <div>Прицеливание может осуществляться как одним, так и двумя глазами, что создает для стрелка ничем не
                ограниченное поле зрения. Прицеливание может осуществляться как одним, так и двумя глазами, что создает
                для стрелка ничем не ограниченное поле зрения. Прицеливание может осуществляться как одним, так и двумя
                глазами, что создает для стрелка ничем не ограниченное поле зрения.
            </div>
        </div>
        <div class="col_2">
            <h1>Связаться с нами:</h1>

            <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'message-form',
                    'action'=>Yii::app()->createUrl('//about'),
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation'=>false,
                    'htmlOptions'=>array(
                        'class' => 'contacts',
                        'validateOnSubmit' => true,
                    ),
                )); ?>

            <?php echo $form->errorSummary($model); ?>

            <div class="row">
                <?php echo $form->labelEx($model,'full_name'); ?>

                <div>
                    <?php echo $form->textField($model,'full_name',array('size'=>30,'maxlength'=>30)); ?>
                    <?php echo $form->error($model,'full_name'); ?>
                </div>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'institution'); ?>

                <div>
                    <?php echo $form->dropDownList($model, 'institution', $model->getInstitutionList()); ?>
                    <?php echo $form->error($model,'institution'); ?>
                </div>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'phone'); ?>

                <div>
                    <?php echo $form->textField($model,'phone',array('size'=>15,'maxlength'=>15)); ?>
                    <?php echo $form->error($model,'phone'); ?>
                </div>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'email'); ?>

                <div>
                    <?php echo $form->textField($model,'email',array('size'=>30,'maxlength'=>30)); ?>
                    <?php echo $form->error($model,'email'); ?>
                </div>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'issue'); ?>

                <div>
                    <?php echo $form->dropDownList($model, 'issue', $model->getIssueList()); ?>
                    <?php echo $form->error($model,'issue'); ?>
                </div>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'message'); ?>

                <div>
                    <?php echo $form->textArea($model,'message',array('rows'=>6, 'cols'=>50)); ?>
                    <?php echo $form->error($model,'message'); ?>
                </div>
            </div>
            <div class="tar">
                <?php echo CHtml::submitButton('Отправить'); ?>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
