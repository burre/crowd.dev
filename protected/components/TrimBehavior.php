<?php
/**
 * Created by PhpStorm.
 * User: burre
 * Date: 22.10.14
 * Time: 14:25
 */

class TrimBehavior extends CActiveRecordBehavior {

    /**
     * @param $field
     * @param $length
     * @return mixed|null|string|void
     */
    public function trim($field, $length) {
        if (is_string($this->owner->$field)) {
            $string = $this->owner->$field;
            if (strlen($string) > $length) {
                $string = substr($string, 0, $length);
                $lastSymbol = substr($string, -1, 1);
                if (!in_array($lastSymbol, ['.', ',', '='])) {
                    $string .= '...';
                } else {
                    substr_replace($string, '...', -1, 1);
                }
            }
            return $string;
        } else {
            return $this->owner->$field;
        }
    }

} 