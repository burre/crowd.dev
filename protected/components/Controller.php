<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	//public $layout='//layouts/column1';
    // задается в config/mail.php

	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    public $customPageTitle = '';

    /**
     * Here we include client JS and CSS files.
     *
     * @param CAction $action
     *
     * @return bool
     */
    protected  function beforeAction($action)
    {
        if( parent::beforeAction($action) ) {
            $this->initClientScripts();

            return true;
        }
        return false;
    }

    protected  function beforeRender() {
        $this->setPageTitle(Yii::app()->name . ' - ' . $this->customPageTitle);
        return true;
    }

    private function initClientScripts()
    {
        /* @var $cs CClientScript */
        $cs = Yii::app()->clientScript;
        $base = Yii::app()->request->baseUrl;
        // local copy of jQuery
        //$cs->registerScriptFile($base . '/js/jquery-1.11.0.js' );
        $cs->registerPackage('jquery');
        $cs->registerScriptFile($base . '/js/jquery.cycle2.js', CClientScript::POS_END);
        $cs->registerScriptFile($base . '/js/slider.js', CClientScript::POS_END);

        $cs->registerCssFile($base . '/css/style.css');
        $cs->registerCssFile('http://fonts.googleapis.com/css?family=Cuprum:700,400&subset=latin,cyrillic');
    }

}