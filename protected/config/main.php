<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath'   => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name'       => 'Допомагаю нашим',
    'layoutPath' => 'protected/views/layouts',
    'layout'     => 'column1',
    // preloading 'log' component
    'preload'    => array('log'),
    // autoloading model and component classes
    'import'     => array(
        'application.models.*',
        'application.components.*',
        //        'application.components.common.*',
    ),
    'modules'    => array(
        'admin'      => array(
            'layoutPath' => 'protected/modules/admin/views/layouts',
            'layout'     => 'column2',
            'import'     => array(
                'application.modules.admin.components.*',
            ),
        ),
        // uncomment the following to enable the Gii tool

        'gii'        => array(
            'class'     => 'system.gii.GiiModule',
            'password'  => 'qwe123',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
        'hybridauth' => array(
            'baseUrl'     => 'http://' . $_SERVER['SERVER_NAME'] . '/hybridauth',
            'withYiiUser' => false, // Set to true if using yii-user
            "providers"   => array(
                "google"   => array(
                    "enabled" => true,
                    "keys"    => array("id" => "", "secret" => ""),
                    "scope"   => ""
                ),
                "facebook" => array(
                    "enabled" => true,
                    "keys"    => array("id" => "", "secret" => ""),
                    "scope"   => "email,publish_stream",
                    "display" => ""
                ),
                "twitter"  => array(
                    "enabled" => true,
                    "keys"    => array("key" => "", "secret" => "")
                ),
                "openid"   => array(
                    "enabled" => true
                ),
            )
        ),
    ),
    // application components
    'components' => array(
        'session'      => array(
            'autoStart' => true,
        ),
        'user'         => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager'   => array(
            'showScriptName' => false,
            'urlFormat'      => 'path', // убирает index.php
            'rules'          => array(
                ''                                                    => array('site/index', 'urlSuffix' => ''),
                '<action:login|logout|register>'                      => 'site/<action>',
                '<action:about|join>'                                 => 'site/<action>',
                '<controller:project>/<status:inprogress|new>'        => 'project/index',
                '<controller:project>/<action:view>/id/<id:\d+>'      => 'project/view',
                '<controller:project>'                                => 'project/index',
                '<controller:report>'                                 => 'report/index',
                '<controller:report>/<action:view>/id/<id:\d+>'      => 'report/view',
                '<module:\w+>'                                        => '<module>/default/index',
                '<module:\w+>/<controller:\w+>'                       => '<module>/<controller>/index',
                '<module:\w+>/<controller:\w+>/<id:\d+>'              => '<module>/<controller>/view',
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>'          => '<module>/<controller>/<action>',
            ),
        ),
        'cache' => array(
//             'class' => 'system.caching.CFileCache', // for production
            'class' => 'system.caching.CDummyCache',  // for developing
        ),
        // uncomment the following to use a MySQL database
        'db'           => array(
            'connectionString' => 'mysql:host=localhost;dbname=crowd_dev',
            'emulatePrepare'   => true,
            'username'         => 'root',
            'password'         => 'qwe123',
            'charset'          => 'utf8',
            'enableProfiling'  => true,
            'enableParamLogging' => true,
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log'          => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                array(
                    'class'  => 'CFileLogRoute',
                    // 'levels' => 'error, warning',
                    'levels' => 'error, warning, trace, log',
                    'categories' => 'system.db.CDbCommand',
                    'logFile' => 'db.log',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),
        // replace built-in package 'jquery' with newest version
        'clientScript'=>array(
            'packages'=>array(
                'jquery'=>array(
                    'baseUrl'=>'http://ajax.googleapis.com/ajax/libs/jquery/',
                    'js'=>array('1.11.1/jquery.min.js'),
                )
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'     => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@crowd.com',
        'uploadDirRewards' => 'upload/rewards',
        'uploadDirPartners' => 'upload/partners',
        'uploadDirProjects' => 'upload/projects',
        'projectsOnHomePage' => 4,
    ),
);