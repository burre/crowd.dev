<?php

/**
 * This is the model class for table "rewards".
 *
 * The followings are the available columns in table 'rewards':
 * @property integer $id
 * @property integer $min_pay
 * @property string $description
 * @property string $images
 *
 * The followings are the available model relations:
 * @property Payments[] $payments
 * @property ProjectsRewards[] $projectsRewards
 */
class Reward extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rewards';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('name', 'required'),
            array('name', 'length', 'max'=>50),
            array('min_pay', 'required'),
            array('min_pay', 'numerical', 'integerOnly'=>true),
			array('images', 'length', 'max'=>150),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, min_pay, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'payments' => array(self::HAS_MANY, 'Payments', 'reward_id'),
			'projectsRewards' => array(self::HAS_MANY, 'ProjectsRewards', 'reward_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'min_pay' => 'Min Pay',
			'description' => 'Description',
			'images' => 'Images',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name);
		$criteria->compare('min_pay',$this->min_pay);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('images',$this->images,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reward the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Returns list of rewards' names for dropdown list.
     *
     * @return array
     */
    public function getRewardsList()
    {
        $all = $this->findAll();

        return CHtml::listData($all, 'id', 'name');
    }
}
