<?php

/**
 * This is the model class for table "payments".
 *
 * The followings are the available columns in table 'payments':
 * @property integer $id
 * @property string $sum
 * @property string $status
 * @property string $card4digits
 * @property integer $project_id
 * @property integer $user_id
 * @property integer $reward_id
 * @property integer $pay_method_id
 * @property integer $delivery_method_id
 * @property string $reward_status
 * @property string $comments
 *
 * The followings are the available model relations:
 * @property Projects $project
 * @property Users $user
 * @property Rewards $reward
 * @property PayMethods $payMethod
 * @property DeliveryMethods $deliveryMethod
 */
class Payment extends CActiveRecord
{
    const REWARD_NOTSENT = 'notsent';
    const REWARD_SENT = 'sent';
    const REWARD_PROCESSING = 'processing';
    const REWARD_REFUSED = 'refused';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id, status, card4digits, user_id, reward_id, pay_method_id, delivery_method_id', 'numerical', 'integerOnly'=>true),
			array('sum, reward_status', 'length', 'max'=>10),
            array('card4digits', 'length', 'max'=>4),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sum, status, card4digits, project_id, user_id, reward_id, pay_method_id, delivery_method_id, reward_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'reward' => array(self::BELONGS_TO, 'Rewards', 'reward_id'),
			'payMethod' => array(self::BELONGS_TO, 'PayMethods', 'pay_method_id'),
			'deliveryMethod' => array(self::BELONGS_TO, 'DeliveryMethods', 'delivery_method_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sum' => 'Сумма',
            'status' => 'Статус',
            'card4digits' => 'Последние 4 цифры карты',
			'project_id' => 'Проект',
			'user_id' => 'Пользователь',
			'reward_id' => 'Награда',
			'pay_method_id' => 'Метод платежа',
			'delivery_method_id' => 'Метод доставки',
			'reward_status' => 'Статус награды',
            'comments' => 'Примечания',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sum',$this->sum,true);
		$criteria->compare('card4digits',$this->card4digits,true);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('reward_id',$this->reward_id);
		$criteria->compare('pay_method_id',$this->pay_method_id);
		$criteria->compare('delivery_method_id',$this->delivery_method_id);
		$criteria->compare('reward_status',$this->reward_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Payment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
