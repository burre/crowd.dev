<?php

/**
 * Project: HelpingOur (crowd.dev)
 * File:    DonateForm.php
 * Date:    26.09.14 @ 13:10
 *
 * Author:  Victor Burak <victor.burre@gmail.com>
 */
class DonateForm extends CFormModel
{

    public $name;
    public $city;
    public $address;
    public $card4digits;
    public $email;
    public $phone;
    public $comments;
    public $sum;
    public $paymethod = '';
    public $shipmethod = '';
    public $project_id;
    public $user_id;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            // name, email, subject and body are required
            array('project_id, name, city, address, card4digits, email, phone, sum, paymethod, shipmethod', 'required'),
            // email has to be a valid email address
            array('email', 'email'),
            array('card4digits, project_id', 'numerical', 'integerOnly' => true),
            array('card4digits', 'length', 'max' => 4),
            array('sum', 'length', 'max' => 10),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'name'        => 'П.І.Б.',
            'city'        => 'Місто',
            'address'     => 'Індекс та адреса',
            'card4digits' => 'Останні 4 цифри платіжної карти',
            'email'       => 'Электронна пошта',
            'phone'       => 'Телефон',
            'comments'    => 'Додаткова інформація',
            'sum'         => 'Я хочу підтримати на суму',
            'project_id'  => 'ID проекта',
        );
    }

    /**
     * @return bool
     */
    public function saveAsPayment()
    {
        $payMethods = array(
            'portmone'    => 1,
            'cash'        => 2,
            'on_delivery' => 3,
        );

        $shipMethods = array(
            'ukrpost' => 1,
            'newpost' => 2,
            'himself' => 3,
            'cancel'  => 4,
        );

        $user = User::model()->findByAttributes(array('email' => $this->email));

        if (!$user) {
            $name = explode(' ', $this->name);
            $user = new User();
            $user->first_name = isset($name[0]) ? $name[0] : $this->email;
            $user->last_name = isset($name[1]) ? $name[1] : $this->email;
            $user->city =  $this->city;
            $user->address = $this->address;
            $user->email = $this->email;
            $user->phone = $this->phone;

            $user->save();
        }

        $payment                     = new Payment();
        $payment->sum                = $this->sum;
        $payment->card4digits        = $this->card4digits;
        $payment->project_id         = (int)$this->project_id;
        $payment->pay_method_id      = $payMethods[$this->paymethod];
        $payment->delivery_method_id = $shipMethods[$this->shipmethod];
        $payment->reward_status      = Payment::REWARD_NOTSENT;
        $payment->reward_id          = null;
        $payment->status             = 0;
        $payment->comments           = $this->comments;
        $payment->user_id            = $user->id;
        if ($payment->delivery_method_id != 4) {
            // выбрать награды текущего преокта
            // выбрать подходящую
            //$payment->reward_id =
        }
        $payment->save();

        return true;
    }
} 