<?php

/**
 * This is the model class for table "projects".
 *
 * The followings are the available columns in table 'projects':
 *
 * @property integer           $id
 * @property string            $title
 * @property string            $description
 * @property string            $images
 * @property string            $for_whom
 * @property string            $destination
 * @property string            $target_sum
 * @property string            $start_date
 * @property string            $end_date
 * @property string            $status
 * @property string            $comments
 * @property string            $videos
 *
 * The followings are the available model relations:
 * @property Payments[]        $payments
 * @property ProjectPartners[] $projectPartners
 * @property ProjectRewards[]  $projectRewards
 */
class Project extends CActiveRecord
{
    const STATUS_DRAFT = 'draft';
    const STATUS_IN_PROGRES = 'inprogres';
    const STATUS_COMPLETED = 'completed';
    const STATUS_EARLY = 'early';
    const STATUS_ARCHIVED = 'archived';
    const STATUS_DELETED = 'deleted';

    private static $enabledStatuses = ['inprogress', 'completed', 'early', 'archived'];
    public $totalSum = 0;
    public $totalPercentage = 0;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'projects';
    }

    public function afterFind()
    {
        $this->totalSum        = $this->getCollectedSum();
        $this->totalPercentage = $this->getCollectedPercentage();
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return array(
            'trimBehavior' => array(
                'class' => 'TrimBehavior',
            ),
        );
    }

    /**
     * Conver array of pathes to JSON before savng.
     *
     * @return bool
     */
    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if (is_array($this->images)) {
                $this->images = json_encode($this->images);
            }
        }
        return true;
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title', 'required'),
            array('title', 'length', 'max' => 100),
            array('target_sum, status', 'length', 'max' => 10),
            array('description, images, for_whom, destination, start_date, end_date, comments', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array(
                'id, title, description, for_whom, destination, target_sum, start_date, end_date, status, comments',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'payments'        => array(self::HAS_MANY, 'Payment', 'project_id'),
            'projectPartners' => array(self::HAS_MANY, 'ProjectPartners', 'project_id'),
            'projectRewards'  => array(self::HAS_MANY, 'ProjectRewards', 'project_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'          => 'ID',
            'title'       => 'Заголовок',
            'description' => 'Описание',
            'images'      => 'Изображения',
            'for_whom'    => 'Для кого',
            'destination' => 'Назначение',
            'target_sum'  => 'Сумма  ',
            'start_date'  => 'Дата начала',
            'end_date'    => 'Дата завершения',
            'status'      => 'Статус',
            'comments'    => 'Примечания',
            'videos'      => 'Видео',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('images', $this->images, true);
        $criteria->compare('for_whom', $this->for_whom, true);
        $criteria->compare('destination', $this->destination, true);
        $criteria->compare('target_sum', $this->target_sum, true);
        $criteria->compare('start_date', $this->start_date, true);
        $criteria->compare('end_date', $this->end_date, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('comments', $this->comments, true);

        return new CActiveDataProvider(
            $this, array(
                'criteria' => $criteria,
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return Project the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public static function getEnabledStatuses()
    {
        return self::$enabledStatuses;
    }

    /**
     * @param string|array  $status
     * @param string        $order
     *
     * @return CActiveRecord[]
     */
    public static function getAllByStatus($status, $order = "DESC")
    {
        $criteria = new CDbCriteria();
        if (is_array($status)) {
            $criteria->addInCondition('status', $status);
        } else {
            $criteria->condition = 'status=:status';
            $criteria->params = ['status' => $status];
        }
        $criteria->order = 'start_date ' . $order;

        return Project::model()->findAll($criteria);
    }

    /**
     * @return mixed
     */
    public function getCollectedSum()
    {
        return Yii::app()->db->createCommand()
            ->select('SUM(sum) as total')
            ->from('payments')
            ->where('project_id=:project_id', array('project_id' => $this->id))
            ->queryScalar();
    }

    /**
     * @return float
     */
    public function getCollectedPercentage()
    {
        if ($this->target_sum > 0)
            return number_format(($this->getCollectedSum() / $this->target_sum) * 100, 2);
        else
            return 0;
    }

    /**
     * @return integer
     */
    public function getMemberCount()
    {
        return Payment::model()->countByAttributes(array('project_id' => $this->id));
    }

    /**
     * @return CActiveRecord[]
     * @throws CDbException
     */
    public function getAllPartners()
    {
        $partners = $this->getRelated('projectPartners');
        $ids      = array();
        foreach ($partners as $p) {
            $ids[] = $p->partner_id;
        }
        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', $ids);

        return Partner::model()->findAll($criteria);
    }

    /**
     * @return CActiveRecord[]
     * @throws CDbException
     */
    public function getAllRewards()
    {
        $rewards = $this->getRelated('projectRewards');
        $ids     = array();
        foreach ($rewards as $r) {
            $ids[] = $r->reward_id;
        }
        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', $ids);
        $criteria->order = 'min_pay ASC';

        return Reward::model()->findAll($criteria);
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images = json_decode($this->images);
    }

    /**
     * @return mixed
     */
    public function getVideos()
    {
        return $this->videos = json_decode($this->videos);
    }
}
