/**
 * Created by burre on 09.09.14.
 */
$(document).ready(function () {

    $('.projects_slider').cycle({
        slides: "div.slide",
        fx: "scrollHorz",
        prev: "#slide_prev",
        next: "#slide_next",
        paused: true
    });

    $('.projects_slider').on('cycle-next', function (event, optionHash) {
        var index = optionHash.currSlide;
        var slideCur = $("div.projects_slider div.slide.cycle-slide-active").find("div.project.completed").length;
        var slideNum = parseInt($("div#slide_caption span.slideNum").html());
        var hiddenSlides = $("div.projects_slider div.cycle-sentinel div.project.completed").length;
        var slideCount = parseInt($("div#slide_caption span.slideCount").html());

        slideNum += slideCur;
        if (slideNum > slideCount) {
            slideNum = hiddenSlides;
        }
        $("div#slide_caption span.slideNum").html(slideNum);
        return false;
    });

    $('.projects_slider').on('cycle-prev', function (event, optionHash) {
        var index = optionHash.currSlide;
        var slideCur = $("div.projects_slider div.slide.cycle-slide-active").find("div.project.completed").length;
        var slideNum = parseInt($("div#slide_caption span.slideNum").html());
        var hiddenSlides = $("div.projects_slider div.cycle-sentinel div.project.completed").length;
        var slideCount = parseInt($("div#slide_caption span.slideCount").html());
        if (slideNum != slideCount) {
            slideNum -= slideCur;
            if (slideNum < hiddenSlides) {
                slideNum = slideCount;
            }
        } else {
            slideNum = hiddenSlides;
        }
        $("div#slide_caption span.slideNum").html(slideNum);
        return false;
    });

    $("div.popup a.close, div.mask, div.flash_msg a.close").click(function () {
        $("div.mask").fadeOut(250);
        $("div.popup").fadeOut(250);
        $("div.flash_msg").fadeOut(250);
        return false;
    });

});

$(document).on('cycle-initialized', '.projects_slider', function (event, optionHash) {
    var slideCount = $("div.projects_slider div.project.completed").length;
    var hiddenSlides = $("div.projects_slider div.cycle-sentinel div.project.completed").length;
    slideCount = slideCount - hiddenSlides;
    var firstSlide = $("div.projects_slider div.slide").first().find("div.project.completed").length;
    $("div#slide_caption span.slideCount").html(slideCount);
    $("div#slide_caption span.slideNum").html(firstSlide);
});
